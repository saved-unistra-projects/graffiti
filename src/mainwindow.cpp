#include "mainwindow.h"

MainWindow::MainWindow(QWidget *parent) : QMainWindow(parent)
{
  setWindowTitle("Graffiti");
  setWindowIcon(QIcon(":/Images/favicon.png"));
  //this->showFullScreen();

  // Main area
  m_centralArea = new QWidget;
  setCentralWidget(m_centralArea);


  m_mainLayout = new QHBoxLayout(m_centralArea);


  // Left area (main)
  m_leftLayout = new QVBoxLayout;
  m_leftLayoutChild = new QHBoxLayout;
  m_layoutDbSelectShow = new QVBoxLayout;

  m_graffitiArea = new Painter;
  m_graffitiArea->setMinimumHeight(this->height()/2);

  m_database = new QGroupBox(tr("Database :"));
  m_layoutDatabase = new QVBoxLayout;
  m_layoutDatabaseRow = new QGridLayout;
  m_layoutDatabaseRow->setHorizontalSpacing(10);
  m_alphabetic = new QRadioButton;
  m_alphabetic->setChecked(true);
  this->setDbRep(std::string(DB_DIR) + "alphabetic/");
  this->loadDB();
  m_numeric = new QRadioButton;

  m_alphabeticLabel = new QLabel(tr("&alphabetic"));
  m_alphabeticLabel->setBuddy(m_alphabetic);
  m_numericLabel = new QLabel(tr("&numeric"));
  m_numericLabel->setBuddy(m_numeric);

  m_layoutDatabaseRow->addWidget(m_alphabetic, 0, 0);
  m_layoutDatabaseRow->addWidget(m_alphabeticLabel, 0, 1);
  m_layoutDatabaseRow->addWidget(m_numeric, 1, 0);
  m_layoutDatabaseRow->addWidget(m_numericLabel, 1, 1);

  m_layoutDatabase->addLayout(m_layoutDatabaseRow);
  m_database->setLayout(m_layoutDatabase);

  m_showAlphabet = new QPushButton(tr("❬❬ Show Alphabet"));
  m_showAlphabet->setCheckable(true);

  m_layoutDbSelectShow->addWidget(m_database);
  m_layoutDbSelectShow->addWidget(m_showAlphabet);
  m_leftLayoutChild->addLayout(m_layoutDbSelectShow);

  m_layoutHistoAddDb = new QVBoxLayout;

  m_histo = new Histo;

  m_layoutHistoAddDb->addWidget(m_histo);
  m_leftLayoutChild->addLayout(m_layoutHistoAddDb);

  m_leftLayout->addWidget(m_graffitiArea);
  m_leftLayout->addLayout(m_leftLayoutChild);


#ifdef ADDDB 
    m_groupeAddDb = new QGroupBox("Ajouter à la database");
    m_groupeAddDb->setCheckable(true);
    m_groupeAddDb->setChecked(false);
    m_layoutAddDb = new QFormLayout;
    m_nameDb = new QLineEdit;
    m_layoutAddDb->addRow("&Path", m_nameDb);
    m_groupeAddDb->setLayout(m_layoutAddDb);
    m_layoutHistoAddDb->addWidget(m_groupeAddDb);
#endif


  // Right Area (main)
  m_rightLayout = new QVBoxLayout;

  m_showText = new QTextEdit;
  m_showText->setReadOnly(true);
  m_showText->setDocumentTitle("Texte : ");
  QPalette palette;
  palette.setColor(QPalette::Base, QColor(0, 12, 24));
  palette.setColor(QPalette::Text, QColor(102, 136, 204));
  m_showText->setPalette(palette);
  QFont font = m_showText->font();
  font.setPointSize(25.0);
  m_showText->setFont(font);

  m_rightLayout->addWidget(m_showText);



  // Formatting
  m_mainLayout->addLayout(m_leftLayout);
  m_mainLayout->addLayout(m_rightLayout);

  // Bottom area
  m_alphabetArea = new QDockWidget(tr("Alphabet"), this);
  m_alphabetArea->setFeatures(QDockWidget::DockWidgetMovable);
  m_alphabetArea->hide();
  m_image = new QLabel;
  m_image->setPixmap(QPixmap(":Images/Palm_Graffiti_alphabetic_gestures.png"));
  m_alphabetArea->setWidget(m_image);
  addDockWidget(Qt::LeftDockWidgetArea, m_alphabetArea);

  // Window state
  this->m_timer = new QTimer();
  this->m_timer->setSingleShot(true);
  this->ignore = false;
  this->timerEnded = false;
  this->setVerrMaj(false);
  this->setLastMaj(false);
  this->m_graffitiArea->setNbStrokes(0);
  std::vector<std::string> oldsteps;
  this->oldSteps = oldsteps;

  // Signals
  QObject::connect(m_showAlphabet, SIGNAL(clicked(bool)), this, SLOT(showAlphabet()));
  QObject::connect(m_alphabetic, SIGNAL(clicked(bool)), this, SLOT(changeAlphabet()));
  QObject::connect(m_numeric, SIGNAL(clicked(bool)), this, SLOT(changeAlphabet()));
  QObject::connect(m_graffitiArea, SIGNAL(hasChanged(bool)), this, SLOT(hypervisor()));
  QObject::connect(m_graffitiArea, SIGNAL(ignoringState(bool)), this, SLOT(setIgnore(bool)));
  QObject::connect(m_timer, SIGNAL(timeout()), this, SLOT(timerHandler()));
}


void MainWindow::changeAlphabet()
{
  // Change recognizing algorithm database


  // Link the right image
  if(m_alphabetic->isChecked())
  {
    m_image->setPixmap(QPixmap(
        ":/Images/Palm_Graffiti_alphabetic_gestures.png"));
    this->freeDB();
    this->setDbRep(std::string(DB_DIR) + "alphabetic/");
    this->loadDB();
  }
  else if(m_numeric->isChecked())
  {
    m_image->setPixmap(QPixmap(
      ":/Images/Palm_Graffiti_numeric_gestures.png"));
    this->freeDB();
    this->setDbRep(std::string(DB_DIR) + "numeric/");
    this->loadDB();
  }
}

void MainWindow::showAlphabet()
{
  if(m_showAlphabet->isChecked())
  {
    m_showAlphabet->setText(tr("❭❭ Hide Alphabet"));
    m_alphabetArea->show();
  }
  else
  {
    m_alphabetArea->hide();
    m_showAlphabet->setText(tr("❬❬ Show Alphabet"));
  }
}

void MainWindow::resizeEvent(QResizeEvent *event)
{
  QWidget::resizeEvent(event);
}


#ifdef ADDDB
void MainWindow::addGrilleDb(GrilleScaled* gs)
{
  QString path = QString::fromStdString(this->getDbRep())
    + "examples_" + m_nameDb->text() + ".txt";
  Database db(path.toStdString());
  std::vector<std::vector<Case>> grille;
  std::vector<Case> row;
  for(size_t i = 0; i < GrilleScaled::getSizeY();i++){
    for(size_t j = 0; j < GrilleScaled::getSizeX();j++){
      if(gs->getCase(j,i)->getProba()!=0)
        row.push_back(Case(1));
      else
        row.push_back(Case(0));
      }
      grille.push_back(row);
      row.clear();
    }
  GrilleScaled *copy = new GrilleScaled(grille);
  db.add_grille(copy);
}
#endif


void MainWindow::setIgnore(bool b)
{
  this->ignore = b;
}

bool MainWindow::getIgnore()
{
  return this->ignore;
}

void MainWindow::setDbRep(std::string db)
{
  this->dbRep = db;
}

std::string MainWindow::getDbRep()
{
  return this->dbRep;
}

bool MainWindow::getVerrMaj()
{
  return this->verrMaj;
}

void MainWindow::setVerrMaj(bool b)
{
  this->verrMaj = b;
}

bool MainWindow::getLastMaj()
{
  return this->lastMaj;
}

void MainWindow::setLastMaj(bool b)
{
  this->lastMaj = b;
}


void MainWindow::loadDB(){
  std::string path = this->getDbRep();
  std::string full_path;
  QDir d(QString::fromStdString(path));
  if (!d.exists())
  {
    throw std::system_error(errno, std::generic_category(),
	    "problème de lecture du répertoire");
  }


  QStringList filters;
  filters << "*.txt";
  d.setNameFilters(filters);
 
  QStringList entry = d.entryList();
  size_t i, size = d.count();

  for(i=0; i<size; i++)
  {
    try{
      this->dbv.push_back(new Database(path + std::string("/") + entry.at(i).toStdString()));
    }
    catch(std::invalid_argument* e){
      QMessageBox msgBox;
      msgBox.setText(e->what());
      msgBox.exec();
      return;
    }
  }
}

void MainWindow::freeDB(){
  size_t max_i = this->dbv.size();
  if(max_i != 0){
    for(size_t i = 0; i<max_i;i++){
      delete(this->dbv[i]);
    }
  }
  this->dbv.clear();
}

void MainWindow::hypervisor()
{
  if (this->timerEnded){
    this->timerEnded=false;
    this->hypervisor_end();
    this->oldSteps.clear();
    return;
  }


  // Effacement de l'histogramme
  m_histo->clearBar();

  // transforme le tableau en grilleInput
  GrilleInput gi(m_graffitiArea->getPixels());

  GrilleScaled* gs;
  try{
    gs = gi.scale(); // scale
  }

  catch(std::invalid_argument* e){
      QMessageBox msgBox;
      msgBox.setText(e->what());
      msgBox.exec();
      return;
  }

  // Ajout à la db
#ifdef ADDDB
  if(m_groupeAddDb->isChecked())
  {
    try{
      this->addGrilleDb(gs);
    }
    catch(std::invalid_argument* e){
      QMessageBox msgBox;
      msgBox.setText(e->what());
      msgBox.exec();
      return;
    }
  }
#endif

  // Smooth
  gs->smooth();

  // Créer un tableau des lettres comparées
  this->bm = gs->best_match(this->m_graffitiArea->getNbStrokes(), 
      this->oldSteps, this->dbv);
  this->m_graffitiArea->setNbStrokes(this->m_graffitiArea->getNbStrokes()+1);
  std::vector<std::string> oldsteps;
  for(size_t i=0; i<this->bm.size() && i<SEUIL_WAIT;i++){
    if(std::get<2>(this->bm[i])>0){
      oldsteps.push_back(std::get<0>(this->bm[i]));
      break;
    }
  }
  this->oldSteps = oldsteps;

  bool has_to_wait = false;
  size_t i;
  for (i=0; i<SEUIL_WAIT && !has_to_wait && i<bm.size(); i++)
  {
    has_to_wait = std::get<2>(this->bm[i]) > 0;
  }
  if (has_to_wait)
    this->hypervisor_wait();
  else
    hypervisor_end();
}

void MainWindow::hypervisor_wait()
{
  this->timerEnded = false;
  this->setIgnore(false);
  this->m_timer->start(TIME_WAIT);

}

void MainWindow::hypervisor_end() 
{
  this->oldSteps.clear();
  this->m_graffitiArea->clearImage();

  // Remplir l'histogramme
  ssize_t i, sizeBM = this->bm.size();
  for (i=0; i<sizeBM; i++)
  {
    if(std::get<2>(this->bm[i]) == 0 && std::get<1>(this->bm[i])!=0)
      m_histo->addBar(std::get<0>(this->bm[i]), static_cast<int>(std::get<1>(this->bm[i])*100));
  }
  this->m_graffitiArea->setNbStrokes(0);

  for (i=0; i<sizeBM; i++)
  {
    if(std::get<2>(this->bm[i]) == 0 && std::get<1>(this->bm[i])!=0)
    {
      this->write(std::get<0>(this->bm[i]));
      break;
    }
  }
}

void MainWindow::write(std::string s)
{
  QString str = QString::fromStdString(s);
  if(s.compare("backspace") == 0)
  {
    str = m_showText->toPlainText();
    if(this->getLastMaj())
    {
      int pos = -1;
      int lastSpace = str.lastIndexOf(" ");
      int lastReturn = str.lastIndexOf("\n");
      if(str.contains(" "))
        if(str.contains("\n"))
          pos = lastSpace > lastReturn ? lastSpace : lastReturn;
        else
          pos = lastSpace;
      else if(str.contains("\n"))
        pos = lastReturn;

      str.truncate(pos);
      this->setLastMaj(false);
    }
    else
    {
      str.truncate(str.size()-1);
    }
    m_showText->setPlainText(str);
    return;
  }
  if(s.compare("caps") == 0)
  {
    this->setVerrMaj(!this->getVerrMaj());
    this->setLastMaj(false);
    return;
  }
  if(s.compare("shift") == 0)
  {
    this->setLastMaj(true);
    return;
  }

  if(s.compare("space") == 0)
  {
    str = " ";
  }
  else if(s.compare("return") == 0)
  {
      str = "\n";
  }
  else if(this->getVerrMaj() ^ this->getLastMaj())
  {
    str = str.toUpper();
  }

  m_showText->setPlainText(m_showText->toPlainText() + str);
  this->setLastMaj(false);
}

void MainWindow::timerHandler()
{
  if (!this->getIgnore()){
    this->timerEnded = true;
    this->hypervisor();
  }
  else{
    this->setIgnore(false);
  }
}
