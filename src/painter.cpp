#include <QtWidgets>
#if defined(QT_PRINTSUPPORT_LIB)
#include <QtPrintSupport/qtprintsupportglobal.h>
#if QT_CONFIG(printdialog)
#include <QPrinter>
#endif
#endif

#include "painter.h"

Painter::Painter(QWidget *parent) : QWidget(parent)
{
  scribbling = false;
  myPenWidth = 2;
  myPenColor = Qt::blue;
  background = qRgb(190, 206, 232);
  setNbStrokes(0);
}

std::vector<std::vector<int>> Painter::getPixels()
{
  return this->pixels;
}


void Painter::clearImage()
{
  image.fill(background);
  update();
}

void Painter::mousePressEvent(QMouseEvent *event)
{
    if(event->button() == Qt::LeftButton)
    {
        size_t size = this->pixels.size();
        for(size_t i=0; i<size; i++)
        {
            std::fill(pixels[i].begin(), pixels[i].end(), 0);
	}
        lastPoint = event->pos();
        scribbling = true;
	size_t lpy = static_cast<size_t>(lastPoint.y());
	size_t lpx = static_cast<size_t>(lastPoint.x());
        this->pixels[lpy][lpx] = VALPOINTDEPART;
    }
    emit ignoringState(true);
}


void Painter::mouseMoveEvent(QMouseEvent *event)
{
  if((event->buttons() & Qt::LeftButton) && scribbling)
  {
    if(event->pos().x() >= 0 && event->pos().x() < width() && event->pos().y() >= 0 && event->pos().y() < height())
    {
      drawLineTo(event->pos());
    }
  }
}

void Painter::mouseReleaseEvent(QMouseEvent *event)
{
    if(event->button() == Qt::LeftButton && scribbling)
    {
        scribbling = false;
        emit hasChanged(true);
    }
}



void Painter::paintEvent(QPaintEvent *event)
{
  QPainter painter(this);
  QRect dirtyRect = event->rect();
  painter.drawImage(dirtyRect, image, dirtyRect);
}


void Painter::resizeEvent(QResizeEvent *event)
{
  resizePixels(QSize(width(), height()));
  resizeImage(&image, QSize(width(), height()));
  update();
  QWidget::resizeEvent(event);
}


void Painter::drawLineTo(const QPoint &endPoint)
{
  QPainter painter(&image);
  painter.setPen(QPen(myPenColor, myPenWidth, Qt::SolidLine,
  Qt::RoundCap, Qt::RoundJoin));
  painter.drawLine(lastPoint, endPoint);

  int rad = (myPenWidth / 2) + 2;
  size_t epy = static_cast<size_t>(endPoint.y());
  size_t epx = static_cast<size_t>(endPoint.x());
  update(QRect(lastPoint, endPoint).normalized().
    adjusted(-rad, -rad, +rad, +rad));
  if(pixels[epy][epx] != VALPOINTDEPART)
    pixels[epy][epx] = 1;
  lastPoint = endPoint;
}


void Painter::resizeImage(QImage *image, const QSize &newSize)
{
  if(image->size() == newSize)
  {
    return;
  }

  QImage newImage(newSize, QImage::Format_RGB32);
  newImage.fill(background);
  *image = newImage;
}


void Painter::resizePixels(const QSize &newSize)
{
  size_t i, size = static_cast<size_t>(pixels.size());
  for(i=0; i<size; i++)
  {
    pixels.pop_back();
  }
  for(i=0; i<static_cast<size_t>(newSize.height()); i++)
  {
    std::vector<int> row(static_cast<size_t>(newSize.width()), 0);
    pixels.push_back(row);
  }
}

int Painter::getNbStrokes()
{
  return this->nbStrokes;
}

void Painter::setNbStrokes(int nb)
{
  this->nbStrokes = nb;
}
