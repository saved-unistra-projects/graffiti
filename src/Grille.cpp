#include "Grille.h"

Grille::Grille()
{
  
}

void Grille::setCase(Case c, size_t x, size_t y)
{
  size_t trueY = (y > this->cases.size()-1)? this->cases.size()-1 : y;
  size_t trueX = (x > this->cases[0].size()-1)? this->cases[0].size()-1 : x;

  this->cases[trueY][trueX] = c;
}


Case* Grille::getCase(size_t x, size_t y)
{
  size_t trueY = (y > this->cases.size()-1)? this->cases.size()-1 : y;
  size_t trueX = (x > this->cases[0].size()-1)? this->cases[0].size()-1 : x;

  return &(this->cases[trueY][trueX]);
}


