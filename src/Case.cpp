#include "Case.h"

Case::Case(int pr)
{
  this->setProba(pr);
}

void Case::setProba(int pr)
{
  if(pr > MAX_PROBA)
    this->proba = MAX_PROBA;
  else if(pr < MIN_PROBA)
    this->proba = MIN_PROBA;
  else
    this->proba = pr;
}

int Case::getProba()
{
  return this->proba;
}
