#include "GrilleScaled.h"

using namespace std;

GrilleScaled::GrilleScaled(vector<vector<Case>> grille)
{
  for(size_t i=0; i<grille.size(); i++)
  {
    vector<Case> l;
    for(size_t j=0; j<grille[i].size(); j++)
    {
      Case c = grille[i][j];
      l.push_back(c);
    }
    this->cases.push_back(l);
  }
}

size_t GrilleScaled::getSizeX()
{
  return GrilleScaled::sizeX;
}

size_t GrilleScaled::getSizeY()
{
  return GrilleScaled::sizeY;
}

void GrilleScaled::setHaut(bool h)
{
  this->haut = h;
}

bool GrilleScaled::getHaut()
{
  return this->haut;
}

void GrilleScaled::setDroite(bool d)
{
  this->droite = d;
}

bool GrilleScaled::getDroite()
{
  return this->droite;
}

bool tuple_comp(tuple<string,double,int> a, tuple<string,double,int> b)
{
  return get<1>(a) > get<1>(b);
}

vector<tuple<string,double,int>> GrilleScaled::best_match(int ignore, vector<
    string> steps, vector<Database*> dbv){
  vector<string> parsed_steps;
  string character;
  for(size_t i=0; i < steps.size();i++){
    stringstream ss;
    ss << steps[i];
    while(ss >> character){
      parsed_steps.push_back(character);
    }
  }
  
  tuple<double,int> current_val;
  double current_min = 0;
  double sum = 0;
  bool test;
  vector<tuple<string,double,int>> result;
  size_t max_i = dbv.size();
  Database* db;
  for(size_t i=0; i<max_i;i++){
    db = dbv[i];

	if(db->getPreviousSteps()==ignore){
	    stringstream ss;
	    ss << db->getCharacter();
	    while(ss >> character){
	      if(parsed_steps.size()==0 || (find(parsed_steps.begin(),
		      parsed_steps.end(),character) != parsed_steps.end()))
		test = true;
	    }
	    if(test){
	    current_val = this->compare_database(db);
	    if(get<1>(current_val)){
	      result.push_back(tuple<string, double, int>(
		    db->getCharacter(),
		    get<0>(current_val),
		    db->getRemainingSteps()));
	    }
	  }
	}
	test = false;
      }
    sort(result.begin(), result.end(), tuple_comp);
    max_i = result.size();
    if(max_i > 1){
      current_min = -1 * get<1>(result[max_i-1]);
      for(size_t i=0;i<max_i;i++){
	get<1>(result[i]) += current_min;
	sum += get<1>(result[i]);
      }
      for(size_t i=0;i<max_i;i++){
	get<1>(result[i]) /= sum;
      }
    }
    else if(max_i == 1){ //Score max si une seule lettre
      get<1>(result[0]) = get<1>(result[0])<=0? 
	-1*get<1>(result[0]) + 1 : get<1>(result[0]);
    }
  return result;
}

tuple<double,bool> GrilleScaled::compare_database(Database* db)
{
  GrilleScaled* lecture;
  size_t i;
  if(db->getNombreGrilles()==0){
    return tuple<double,bool>(0.0,false);
  }
  double score = this->compare(db->get_grille_scaled(0));
  double tmp;
  size_t imax = 0;
  size_t ng = db->getNombreGrilles();
  if (db->getVerifH() && (this->getHaut() != db->getHaut()))
    return tuple<double,bool>(0.0,false);
  if (db->getVerifV() && (this->getDroite() != db->getDroite()))
    return tuple<double,bool>(0.0,false);
  for (i=1; i<ng; i++)
  {
    lecture = db->get_grille_scaled(i);
    tmp = this->compare(lecture);
    score = score > tmp ? score : tmp;
    imax = score == tmp ? i : imax;
  }
  return tuple<double,bool>(score,true);
}

double GrilleScaled::compare(GrilleScaled* g)
{
  size_t i, j;
  size_t g_size_x = GrilleScaled::getSizeX();
  size_t g_size_y = GrilleScaled::getSizeY();
  double result = 0;
  size_t nb_g=0;
  size_t nb_t=0;

  for (i=0; i<g_size_x; i++){
    for (j=0; j<g_size_y; j++)
    {
      if (g->getCase(i,j)->getProba() != 0)
      {
	nb_g++;
	if (this->getCase(i,j)->getProba() != 0)
	{
	  result += this->getCase(i,j)->getProba() * 2;
	  nb_t += this->getCase(i,j)->getProba() == 100 ? 1 : 0;
	}
	else
	  result -= MAX_PROBA*9;
      }
      else
      {
	if (this->getCase(i,j)->getProba() != 0)
	{
	  nb_t += this->getCase(i,j)->getProba() == 100 ? 1 : 0;
	  result -= this->getCase(i,j)->getProba();
	}
	else
	  result = result;
      }
    }
  }
  //double malus = nb_g - nb_t > 0 ? nb_g - nb_t : nb_t - nb_g;

  return result;
}

void GrilleScaled::smooth(){
  vector<vector<Case>> grille(GrilleScaled::getSizeY(),
                              vector<Case>(GrilleScaled::getSizeX(), Case(0)));
  GrilleScaled *g = new GrilleScaled(grille);
  Case *c;
  int p;
  for(size_t i = 0;i < GrilleScaled::getSizeY(); i++){
    for(size_t j = 0; j < GrilleScaled::getSizeX();j++){
      c = this->getCase(j, i);
      p = c->getProba();
      if(p!=0){
	smoothie(g, j, i, p/SMOOTH_RATE, 2);
      }
    }
  }

  for(size_t i = 0; i < GrilleScaled::getSizeY(); i++){
    for(size_t j = 0; j < GrilleScaled::getSizeX();j++){
      c = g->getCase(j,i);
      p = c->getProba();
      c = this->getCase(j,i);
      if(p!=0){
	c->setProba(p + c->getProba());
      }
    }
  }
  delete(g);
}




void GrilleScaled::smoothie(GrilleScaled *g, size_t x, size_t y, int value, int iter){
  Case *c;
  int p;
  for(ssize_t i = -1; i < 2; i++){
    for(ssize_t j = -1; j < 2; j++){
      if(j!=0 || i!=0){
	c = g->getCase(x + i, y + j);
	p = c->getProba();
	c->setProba((value > p? value:p));
	if(iter>0){
	  this->smoothie(g, x + i, y + j, value/SMOOTH_RATE, iter-1);
	}
      }
    }
  }
}

QString GrilleScaled::toQString(){
  int max_i = GrilleScaled::getSizeY();
  int max_j = GrilleScaled::getSizeX();
  QString buf="";
  for(int i = 0; i < max_i;i++){
    for(int j = 0; j < max_j; j++){
      buf += QString::number(this->getCase(j,i)->getProba());
    }
      buf += "\n";
  }
  return buf;
}

