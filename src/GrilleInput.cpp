#include "GrilleInput.h"

using namespace std;

GrilleInput::GrilleInput(vector<vector<int>> grille)
{
    size_t x = 0, y = 0;

    size_t nbLignes, nbColonnes;
    nbLignes = grille.size();
    nbColonnes = grille[0].size();
    int proba;
    for(size_t i=0; i<nbLignes; i++)
    {
        vector<Case> l;
        for(size_t j=0; j<nbColonnes; j++)
        {
            if(grille[i][j] != 0)
            {
                proba = MAX_PROBA;
                if(grille[i][j] == VALPOINTDEPART)
                {
                    x = j;
                    y = i;
                }
            }
            else
            {
                proba = 0;
            }
            Case c(proba);
            l.push_back(c);
        }
        this->cases.push_back(l);
    }
    this->setPointDepart(x, y);
}

tuple<size_t, size_t> GrilleInput::getPointDepart()
{
  return this->posPointDepart;
}

void GrilleInput::setPointDepart(size_t x, size_t y)
{
    size_t trueX = (x > this->cases[0].size()-1) ? this->cases[0].size()-1 : x;
    size_t trueY = (y > this->cases.size()-1) ? this->cases.size()-1 : y ;
    this->posPointDepart = tuple<size_t, size_t>(trueX, trueY);
}

ssize_t GrilleInput::get_min_x()
{
  size_t x, y, ysize = this->cases.size(), xsize = this->cases[0].size();
  bool found = false;
  for (x=0; x < xsize && !found; x++)
    for (y=0; y < ysize && !found; y++)
      found = this->getCase(x, y)->getProba() != 0;
  return !found ? -1 : x-1;
}


ssize_t GrilleInput::get_min_y()
{
  size_t x, y, ysize = this->cases.size(), xsize = this->cases[0].size();
  bool found = false;
  for (y=0; y < ysize && !found; y++)
    for (x=0; x < xsize && !found; x++)
      found = this->getCase(x, y)->getProba() != 0;
  return !found ? -1 : y-1;
}


ssize_t GrilleInput::get_max_x()
{
  size_t x, y, ysize = this->cases.size(), xsize = this->cases[0].size();
  bool found = false;
  for (x=xsize; x>0 && !found; x--)
    for (y=0; y<ysize && !found; y++)
      found = this->getCase(x-1, y)->getProba() != 0;
  return !found ? -1 : x+1;
}


ssize_t GrilleInput::get_max_y()
{
  size_t x, y, ysize = this->cases.size(), xsize = this->cases[0].size();
  bool found = false;
  for (y=ysize; y>0 && !found; y--)
    for (x=0; x<xsize && !found; x++)
      found = this->getCase(x, y-1)->getProba() != 0;
  return !found ? -1 : y+1;
}

GrilleScaled* GrilleInput::scale()
{
  ssize_t min_y = this->get_min_y();
  ssize_t min_x = this->get_min_x();
  ssize_t max_y = this->get_max_y();
  ssize_t max_x = this->get_max_x();

  ssize_t diff = (max_x - min_x) > (max_y - min_y) ?
    (max_x - min_x) : (max_y - min_y);



  double coeff_x = static_cast<double>(diff)
                 / static_cast<double>(GrilleScaled::getSizeX());
  double coeff_y = static_cast<double>(diff)
                 / static_cast<double>(GrilleScaled::getSizeY());

  ssize_t offset_x = (GrilleScaled::getSizeX() - (max_x - min_x)/coeff_x)/2;
  ssize_t offset_y = (GrilleScaled::getSizeY() - (max_y - min_y)/coeff_y)/2;

  double coord_y;
  double coord_x;
  Case* val;

  vector<vector<Case>> grille(GrilleScaled::getSizeY(),
                              vector<Case>(GrilleScaled::getSizeX(), Case(0)));

  if(max_y == -1 || max_x == -1)
  {
    throw invalid_argument("On ne peut redimmensionner que les grilles \
			    non vides !");
  }

  for(ssize_t i = min_y; i<max_y; i++)
  {
    for(ssize_t j = min_x; j<max_x; j++)
    {
      val = this->getCase(j, i);
      if(val->getProba() > 0)
      {
        coord_x = (j - min_x) / coeff_x + offset_x;
        coord_y = (i - min_y) / coeff_y + offset_y;
        grille[static_cast<size_t>(coord_y)][static_cast<size_t>(coord_x)]=*val;
      }
    }
  }
  GrilleScaled* g = new GrilleScaled(grille);

  double coord_x_origin =
    (get<0>(this->getPointDepart()) - min_x) / coeff_x + offset_x;
  double coord_y_origin =
    (get<1>(this->getPointDepart()) - min_y) / coeff_y + offset_y;
  g->setDroite(static_cast<size_t>(coord_x_origin)
    > (GrilleScaled::getSizeX()/2));
  g->setHaut(static_cast<size_t>(coord_y_origin)
    < (GrilleScaled::getSizeY()/2));
  return g;
}
