#include "histo.h"

QT_CHARTS_USE_NAMESPACE

Histo::Histo(QWidget *parent) : QWidget(parent)
{
  series = new QBarSeries;
  valeurs = new QBarSet("Lettres");
  this->series->append(valeurs);
  
  chart = new QChart();
  chart->addSeries(series);
  chart->setTitle("Certitudes");
  chart->setAnimationOptions(QChart::SeriesAnimations);

  categories = new QStringList;
  axisX = new QBarCategoryAxis();
  chart->addAxis(axisX, Qt::AlignBottom);
  series->attachAxis(axisX);

  axisY = new QValueAxis();
  axisY->setRange(0,100);
  chart->addAxis(axisY, Qt::AlignLeft);
  series->attachAxis(axisY);

  chart->legend()->setVisible(true);
  chart->legend()->setAlignment(Qt::AlignBottom);

  chartView = new QChartView(chart);
  chartView->setRenderHint(QPainter::Antialiasing);
  chartView->chart()->setTheme(QChart::ChartThemeDark);

  layoutPainter = new QHBoxLayout;
  layoutPainter->addWidget(chartView);
  this->setLayout(layoutPainter);
}

void Histo::addBar(std::string lettre, int val)
{
  this->valeurs->append(val);
  this->categories->append(QString::fromStdString(lettre));
  this->axisX->setCategories(*this->categories);
}

void Histo::clearBar()
{
  this->valeurs->remove(0, this->valeurs->count());
  this->categories->clear();
  this->axisX->clear();
}
