#include "Database.h"

using namespace std;

Database::Database(string path)
{
  this->setSourcePath(path);
  this->init_database();
  this->importFromDB();
}

Database::~Database(){
  size_t max_i = this->gs.size();
  for(size_t i = 0; i<max_i;i++){
    delete(this->gs[i]);
  }
}


void Database::init_database()
{
  QString path = QString::fromStdString(this->getSourcePath());
  QFile db(path);

  if(!db.open(QIODevice::ReadOnly | QIODevice::Text))
  {
    throw new invalid_argument("Impossible de lire la base de données");
  }


    QTextStream in(&db);
    QString line;

    line = in.readLine();
    this->setCharacter(line.toStdString());
    line = in.readLine();
    this->setNombreGrilles(stoi(line.toStdString()));
    line = in.readLine();
    this->setVerifH(stoi(line.toStdString()));
    line = in.readLine();
    this->setVerifV(stoi(line.toStdString()));
    line = in.readLine();
    this->setHaut(stoi(line.toStdString()));
    line = in.readLine();
    this->setDroite(stoi(line.toStdString()));
    line = in.readLine();
    this->setRemainingSteps(stoi(line.toStdString()));
    line = in.readLine();
    this->setPreviousSteps(stoi(line.toStdString()));

    db.close();
}

void Database::setSourcePath(string path)
{
  this->source_path = path;
}

string Database::getSourcePath()
{
  return this->source_path;
}

void Database::setCharacter(string character){
  this->character = character;
}

string Database::getCharacter(){
  return this->character;
}

void Database::setNombreGrilles(int n){
  this->nombre_grilles = n;
}

int Database::getNombreGrilles(){
  return this->nombre_grilles;
}


void Database::setHaut(bool h)
{
  this->haut = h;
}

bool Database::getHaut()
{
  return this->haut;
}

void Database::setDroite(bool d)
{
  this->droite = d;
}

bool Database::getDroite()
{
  return this->droite;
}

void Database::setVerifV(bool v)
{
  this->verifV = v;
}

bool Database::getVerifV()
{
  return this->verifV;
}

void Database::setVerifH(bool h)
{
  this->verifH = h;
}

bool Database::getVerifH()
{
  return this->verifH;
}

void Database::setRemainingSteps(int s){
  this->RemainingSteps = s;
}

int Database::getRemainingSteps(){
  return this->RemainingSteps;
}

void Database::setPreviousSteps(int s){
  this->PreviousSteps = s;
}

int Database ::getPreviousSteps(){
  return this->PreviousSteps;
}


GrilleScaled* Database::get_grille_scaled(int i)
{
  if(i>=this->getNombreGrilles()){
    i=this->getNombreGrilles()-1;
  }
  return this->gs[i];
}

void Database::add_grille(GrilleScaled *g){
  this->gs.push_back(g);
  this->setNombreGrilles(this->getNombreGrilles()+1);
  this->exportToDB();
}

void Database::exportToDB(){
  QString path = QString::fromStdString(this->getSourcePath());
  QFile db(path);
  QTextStream out(&db);

  if(!db.open(QIODevice::WriteOnly | QIODevice::Truncate | QIODevice::Text)){
    throw invalid_argument("Impossible d'écrire dans la base de données");
  }

  out << QString::fromStdString(this->getCharacter()) << endl;
  out << this->getNombreGrilles() << endl;
  out << this->getVerifH() << endl;
  out << this->getVerifV() << endl;
  out << this->getHaut() << endl;
  out << this->getDroite() << endl;
  out << this->getRemainingSteps() << endl;
  out << this->getPreviousSteps() << endl << endl;

  int max_i = this->getNombreGrilles();
  for(int i = 0; i<max_i;i++){
    out << this->get_grille_scaled(i)->toQString() << endl;
  }
  db.close();
}

void Database::importFromDB(){
  QString path = QString::fromStdString(this->getSourcePath());
  QFile db(path);
  QTextStream in(&db);
  QString line;
  string line_str;
  char c;

  vector<Case> vc;
  vector<vector<Case>> vv;

  if(!db.open(QIODevice::ReadOnly | QIODevice::Text))
  {
    throw new invalid_argument("Impossible de lire la base de données");
  }
  for(int i=0;i<META_DATA_LENGTH;i++) // Saute les méta-données
  {
    in.readLine();
  }

  int max_i = this->getNombreGrilles();
  int max_j = GrilleScaled::getSizeX();
  int max_k = max_j;
  for(int i=0;i<max_i;i++){
    for(int j=0;j<max_j;j++){
      line = in.readLine();
      line_str = line.toStdString();
      for(int k = 0; k < max_k; k++)
      {
	c = line_str[k];
	vc.push_back(Case(c - '0'));
      }
      vv.push_back(vc);
      vc.clear();
    }
    this->gs.push_back(new GrilleScaled(vv));
    vv.clear();
    in.readLine();
  }
  db.close();
}
