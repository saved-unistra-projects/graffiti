#-------------------------------------------------
#
# Project created by QtCreator 2019-02-20T18:47:19
#
#-------------------------------------------------

QT       += core gui charts
greaterThan(QT_MAJOR_VERSION, 4): QT += widgets
qtHaveModule(printsupport): QT += printsupport

TARGET = Graffiti
TEMPLATE = app
INCLUDEPATH += $$PWD/include

# The following define makes your compiler emit warnings if you use
# any feature of Qt which has been marked as deprecated (the exact warnings
# depend on your compiler). Please consult the documentation of the
# deprecated API in order to know how to port your code away from it.
DEFINES += QT_DEPRECATED_WARNINGS

# You can also make your code fail to compile if you use deprecated APIs.
# In order to do so, uncomment the following line.
# You can also select to disable deprecated APIs only up to a certain version of Qt.
#DEFINES += QT_DISABLE_DEPRECATED_BEFORE=0x060000    # disables all the APIs deprecated before Qt 6.0.0

CONFIG += c++11

SOURCES += \
        $$files($$PWD/src/*.cpp, true)

HEADERS += \
        $$files($$PWD/include/*.h, true)

RESOURCES += \
        $$files($$PWD/ressources.qrc, true)


release:DESTDIR = bin/release
release:OBJECTS_DIR = obj/release
release:MOC_DIR = moc/release
release:RCC_DIR = rcc/release
release:UI_DIR = ui/release

debug:DESTDIR = bin/debug
debug:OBJECTS_DIR = obj/debug
debug:MOC_DIR = moc/debug
debug:RCC_DIR = rcc/debug
debug:UI_DIR = ui/debug


# Default rules for deployment.
qnx: target.path = /tmp/$${TARGET}/bin
else: unix:!android: target.path = /opt/$${TARGET}/bin
!isEmpty(target.path): INSTALLS += target


# Android parameters
DISTFILES += \
    Images/PageDeGarde.jpg \
    Images/Palm_Graffiti_alphabetic_gestures.png \
    Images/Palm_Graffiti_numeric_gestures.png \
    Images/favicon.png \
    android/AndroidManifest.xml \
    android/build.gradle \
    android/gradle/wrapper/gradle-wrapper.jar \
    android/gradle/wrapper/gradle-wrapper.properties \
    android/gradlew \
    android/gradlew.bat \
    android/res/values/libs.xml \
    databases/alphabetic/examples_+.txt \
    databases/alphabetic/examples_-.txt \
    databases/alphabetic/examples_..txt \
    databases/alphabetic/examples_=.txt \
    databases/alphabetic/examples_?.txt \
    databases/alphabetic/examples_a.txt \
    databases/alphabetic/examples_b.txt \
    databases/alphabetic/examples_backspace.txt \
    databases/alphabetic/examples_c.txt \
    databases/alphabetic/examples_caps1.txt \
    databases/alphabetic/examples_caps2.txt \
    databases/alphabetic/examples_d.txt \
    databases/alphabetic/examples_e.txt \
    databases/alphabetic/examples_f.txt \
    databases/alphabetic/examples_g.txt \
    databases/alphabetic/examples_h.txt \
    databases/alphabetic/examples_i.txt \
    databases/alphabetic/examples_j.txt \
    databases/alphabetic/examples_k.txt \
    databases/alphabetic/examples_l.txt \
    databases/alphabetic/examples_m.txt \
    databases/alphabetic/examples_n.txt \
    databases/alphabetic/examples_o.txt \
    databases/alphabetic/examples_p.txt \
    databases/alphabetic/examples_prefix.txt \
    databases/alphabetic/examples_prefix_ponct.txt \
    databases/alphabetic/examples_q.txt \
    databases/alphabetic/examples_r.txt \
    databases/alphabetic/examples_return.txt \
    databases/alphabetic/examples_s.txt \
    databases/alphabetic/examples_shift.txt \
    databases/alphabetic/examples_smiley_1.txt \
    databases/alphabetic/examples_smiley_2.txt \
    databases/alphabetic/examples_smiley_3.txt \
    databases/alphabetic/examples_smiley_4.txt \
    databases/alphabetic/examples_smiley_5.txt \
    databases/alphabetic/examples_space.txt \
    databases/alphabetic/examples_ss.txt \
    databases/alphabetic/examples_t.txt \
    databases/alphabetic/examples_u.txt \
    databases/alphabetic/examples_v.txt \
    databases/alphabetic/examples_w.txt \
    databases/alphabetic/examples_x.txt \
    databases/alphabetic/examples_y.txt \
    databases/alphabetic/examples_z.txt \
    databases/alphabetic/examples_¡.txt \
    databases/alphabetic/examples_£.txt \
    databases/alphabetic/examples_«.txt \
    databases/alphabetic/examples_°.txt \
    databases/alphabetic/examples_».txt \
    databases/alphabetic/examples_×.txt \
    databases/alphabetic/examples_÷.txt \
    databases/alphabetic/examples_ø.txt \
    databases/alphabetic/examples_‘.txt \
    databases/alphabetic/examples_’.txt \
    databases/numeric/examples_0.txt \
    databases/numeric/examples_1.txt \
    databases/numeric/examples_2.txt \
    databases/numeric/examples_3.txt \
    databases/numeric/examples_4.txt \
    databases/numeric/examples_5.txt \
    databases/numeric/examples_6.txt \
    databases/numeric/examples_7.txt \
    databases/numeric/examples_8.txt \
    databases/numeric/examples_9.txt

contains(ANDROID_TARGET_ARCH,armeabi-v7a) {
    ANDROID_PACKAGE_SOURCE_DIR = \
        $$PWD/android
}

