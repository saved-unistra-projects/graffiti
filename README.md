# Projet de Programmation Orientée Objets

# Détection d'écriture Graffiti

**Dépendances :**

- Bibliothèques Qt et QtCharts.
- qmake

**Dépendances Optionnelles :**

- Bibliothèques android de Qt.

## Compilation

```
qmake
```

```
make
```

**Compiler en mode développeur (ajout possibles à la base de données) :**

```
make DEFINES+=-DADDDB
```

## Fonctionnalités

Cette application permet de reconnaître l'écriture Graffiti.

L'alphabet Grafiti peut être affiché sur le bord gauche de l'écran à l'aide
d'un bouton prévu à cet effet si vous ne connaissez pas cet alphabet.

Il vous suffit ensuite de dessiner une des lettres Graffiti pour que le 
programme la reconnaisse et l'affiche dans la zone de texte à droite de 
l'écran.

Certains caractères spéciaux existent permettants de formatter cette zone de
texte (retour arrière, retour à la ligne, majuscules, etc.).

Afin d'entrer des chiffres, il est nécessaire de sélectionner l'option 
approprié avec les boutons radios à gauche de l'écran.

Notez que vous n'avez pas besoin de particulièrement vous appliquer pour que
le logiciel reconnaisse votre écriture. Vous pouvez également écrire des 
grosses ou petites lettre, le programme devrait s'en sortir. Il ne peut en 
revanche pas encore lire dans votre esprit, il est donc nécessaire de lui fournir
une forme assez précise de votre lettre pour qu'il la reconnaisse.


Enfin, l'application supporte les lettres faites en plusieurs mouvements (telles
que les lettres préfixée de certains symboles, ou même les smileys !)

Si le programme semble parfois attendre que vous entriez un dessin, c'est 
simplement qu'il pensait que ce que vous dessiniez pouvait être le début d'un
lettre en plusieurs mouvements.

## Mode développeur

Le mode développeur permet d'ajouter des grilles dans la base de données.

Celà signifie que vous pouvez faire apprendre au programme de nouvelles lettres,
ou bien modifier sa façon de voir des lettres déjà existantes.

Pour ce faire, une case à cocher se situera en bas de l'écran après avoir 
compilé en mode développeur. Une fois cette case cochée, les prochaines grilles
seront ajoutées à la base de données que vous avez entrée dans le champ de texte
juste en dessous. (sous le nom `examples_<nom_base>.txt`)

Assurez-vous que ce fichier existe dans le dossier 
`database/<lettres_ou_chiffres>`!

Pour créer un fichier d'une nouvelle lettre : veuillez suivre le modèle
suivant :

```
<Caractère représenté>
<Nombre_de_grilles>
<ImportanceH>
<ImportantV>
<Haut>
<Droite>
<StepsRemaining>
<PreviousSteps>
```

<Caractère réprésenté> : Pas forcément un seul caractère, mais peut être une 
chaîne. Séparer plusieurs chaînes par des espaces signifie que la forme 
représentée dans cette base de donnée est le premier mouvement d'un chaîne
de caractères.

<Nombre_de_grille> Ce paramètre est géré par le programme, si vous créez un
nouveau fichier, mettez systèmatiquement 0.

<ImportanceH> La position du premier point dessiné verticalement par rapport au 
dessin est importante (valeur booleenne 1 ou 0).

<ImportanceV> La position du premier point dessiné horizontalement par rapport
au dessin est important (valeur booleenne 1 ou 0).

<Haut> Le premier point est en haut du dessin (0 ou 1).

<Droite> Le premier point est à droite du dessin (0 ou 1).

<StepsRemaining> Nombre d'étapes restantes pour obtenir le caractère complet (
0 pour une lettre en un seul mouvement).

<PreviousSteps> Nombre d'étapes déjà effectuées pour faire ce caractère (
différent de 0 dans le cas des caractères à plusieurs étapes).

La suite du fichier sera remplie par le programme
