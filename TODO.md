Je me charge de créer la branche IHM (Interface Homme Machine) dans laquelle je vais commencer à créer... bah l'IHM...
Pour pouvoir suivre ces changements, il vous faudra installer Qt, je vous laisse aller checker leur [site](https://www.qt.io/).
Je suis obligé de crée un premier ficher sur la branche master pour pouvoir créer une autre branche, d'où la création de cette TODO, je vous laisse la compléter...

# A Faire  

- [x] Faire le premier TP en C++ (et une version qui compile hein !)
- [x] Discuter des différents algorithmes et choisir le meilleur.
- [x] Créer toutes les classes
- [ ] Créer les méthodes :
  - [ ] GrilleInput::scale
  - [ ] GrilleIcaled::compare
  - [ ] GrilleScaled::smoothie
  - [ ] Database::get_grille_scaled
- [ ] Faire la documentation (ex dans GrilleInput.h)

- [x] Créer l'Interface Homme Machine
	- [X] Récupérer un dessin
	- [ ] Vérifier condition dans resizeEvent
	- [ ] Vider pixels lors de chaque effaçage de lettre
	- [ ] Ecrire avec largeur dans pixels
	- [ ] free correctement dans resizePixel
	- [ ] Effaçer les fonction Painter::printPixels + l1 de MainW::changeAlphabet + MainW::resizeEvent + MainW::setShowedText
  - [ ] Optimiser
  	- [ ] Resize = juste effacer diff de taille au vecteur et ajouter difference aussi (si > 0 à chaque fois)
