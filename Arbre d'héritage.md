```mermaid
graph BT;
  Case;
  GrilleScaled --> nd{Grille};
  GrilleInput --> nd{Grille};
  Database;
```

- Case :
  - attribut : 
    - `int proba` : probabilité qu'il y ait un pixel dans la case
      (avec un define MAX_PROBA, MIN_PROBA)


- abstract Grille :
  - attributs :
    - `vector<vector<Case>> cases` : tableau de cases
    - `tuple posPointDepart` : Coordonnées du premier point entré.
    


- GrilleScaled
  - attributs :
    - bool haut; // Le premier point est situé en haut.
    - bool droite; // Le premier point est situé à droite.
  - méthodes :
    - `public double compare(GrilleScaled)` : donne un score de fidélité de
      comparaison entre SCORE_MIN et SCORE_MAX 
    - `private void smoothie()` : étoffe une grille mise à l'échelle
    - `publiic double compare_database(database)` compare une grille avec toutes
      les grilles d'une database
    - `public string best_match(string path)` renvoie le caractère le plus
      approprié parmi ceux présents dans `path`


- GrilleInput
  - méthodes :
    - `public GrilleInput(vector<vector<int>>)` : Constructeur qui transforme
      un vecteur de vecteur d'int en une GrilleInput.
    - `public GrilleScaled scale()` : renvoie une grille mise à l'échelle
      correspondant à la grille brute
    - `public ssize_t get_min_x()` : renvoie la coordonnée verticale minimum de
      l'image
    - `public ssize_t get_max_x()`
    - `public ssize_t get_min_y()`
    - `public ssize_t get_max_y()`


- Database
  - attributs :
    - `string source_path` : chemin du fichier contenant tous les tableaux de
      lettres
    - `nombre_grilles` : nombre de grilles représentées dans le fichier
    - `string character` : caractère à reconnaitre
    - `bool haut` : Le premier point est situé en haut.
    - `bool droite` : Le premier point est situé à droite.
    - `bool verifV` : Importance du placement vertical du premier point.
    - `bool verifH` : Importance du placement horizontal du premier point.

  - méthodes :
    - `public void init_database()` : Initialise la DB depuis le fichier pointé
      par l'attribut source_path.
    - `public GrilleScaled get_grille_scaled(size_t i)` : renvoie la
      GrilleScaled numéro i de la base de données
    - `public void add_grille(GrilleScaled)` : ajoute une GrilleScaled à une
      database

