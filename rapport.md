--- 
title : "Rapport de Projet de POO2"
author: "Raphaël Colin, Michael Paper, Timothée Zerbib"
geometry : margin=3cm
fontsize : 12pt
 
header-includes: 
  \usepackage{graphicx} 
---


\title{
  \begin{center}
      \includegraphics[height=7cm]{Images/PageDeGarde.jpg} \\~\\
  \end{center}
    \begin{center}
    \Huge{Graffiti}
    \end{center}
  \begin{figure}[b]
    \begin{center}
      \includegraphics[height=4cm]{Images/logo.jpeg}
    \end{center}
  \end{figure}
}
\newpage

## Présentation de l'application

L'application permet à l'utilisateur de dessiner un caractère dans une zone
de dessin. Il choisit via une série de boutons radio s'il souhaite entrer un
chiffre ou un autre caractère.
Certains caractères sont écrits en plusieurs mouvements, c'est notamment le cas
du X et du caractère "caps lock". C'est aussi le cas de caractères spéciaux
comme le ß, qui sont préfixés d'une diagonale annonçant que la suite sera
un tel caractère, tout comme les signes de ponctucation qui sont préfixés par
un simple clic.

Nous en avons profité pour ajouter la reconnaissance de certains smileys :)
Cela est permis par la capacité de notre application à reconnaitre des
caractères tapés en un nombre quelconque de mouvements.
Le programme attend une entrée de l'utilisateur lorsqu'il détecte que celui-ci
a entré le premier mouvement d'un caractère à plusieurs traits afin de lui
permettre de le continuer.

Nous avonc réussi à exporter l'application sur téléphones Android.
La version SmartPhone contient précisément les fonctionnalités de la version
"release" sur ordinateur.
Sur ordinateur, il existe une autre version : la version développeur,
permettant l'apprentissage à la volée.

L'interface accompagne l'utilisateur en lui fournissant un modèle des dessins
possibles et ce à quoi ils correpondent.
Lorsque l'utilisateur a fini d'entrer un caractère, le caractère reconnu est
affiché dans une zone de texte, et les caractères de formattage appliquent
leur effet désiré.

Cette interface est également accompagnée d'un histogramme illustrant le
classement des meilleures suppositions de l'algorithme.



## Algorithme

Une première idée d'algorithme a été de définir chaque lettre comme une suite
de segments, mais cet algorithme n'étant pas assez flexible, nous avons
préféré implémenter un algorithme de comparaison de grilles de points.

Lorsque l'utilisateur dessine un trait, celui-ci est mis à une échelle
connue à l'avance afin de permettre la comparaison avec d'autres grilles de
cette taille.
On ne regarde que les grilles qui commencent dans le bon coin (par exemple,
un I et un "maj" seront distingués de cette façon).
Il est ensuite adouci avant d'être comparé à chaque trait d'une série de bases
de données correspondant aux caractères qu'il peut représenter. Cet ensemble
de bases de donées est déterminé par le nombre de trats que l'utilisateur a
déja rentrés et par la zone de saisie dans laquelle il se trouve
(alphabétique/numérique).

Chaque comparaison renvoie un score, et chaque ensemble de comparaisons venant
d'un caractère possible renvoie un score. Le meilleur score de caractère
possible est alors retenu.

Si un caractère parmi les $N$ premiers soit être suivi d'un autre trait, alors
on attend que l'utilisateur réécrive un caractère et on le compare à nouveau de
la même façon, sinon on considère que la meilleure suggestion est la lettre
finie ayant le meilleur score. Ici, nous avons choisi $2$ comme valeur de $N$,
puisqu'il était impossible de discerner le caractère "shift" de la première
partie du caractère "capslock".

Chaque étape de l'algorithme est configurable, ce qui nous a permis de
l'améliorer au cours de l'avancement.

Par exemple, nous voulions au départ que le score donné après la comparaison
à toute une base de données soit la moyenne des comparaisons individuelles,
mais nous avons constaté qu'il était plus efficace de ne garder que le meilleur
score.

Pour comparer deux grilles, on ajoute des points lorsque les grilles ont des
points en commun et on en retire lorsque les grilles diffèrent.



## Choix d'implémentation

Nous avons fait ce projet en C++ à l'aide de la bibliothèque Qt.
Nous avons choisi de développer en C++ car c'est un langage que nous n'avions
pas encore eu l'occasion de pratiquer, mais avec lequel nous serons
probablement rapidement amenés à travailler.

Pour implémenter l'algorithme décrit, nous avons choisi d'utiliser les classes
suivantes :

- Case : Classe contenant une valeur décrite par une case

- GrilleInput : Classe contenant un tableau de Cases à deux dimensions

- GrilleScaled : Classe contenant un tableau de Cases à deux dimensions de
taille fixe

- Database : Classe contenant un vecteur de GrilleScaled obtenues depuis les
bases de données crées

- MainWindow : Classe principale qui supervise les entrées des utilisateurs

- Painter : Correspond à la zone de dessin, elle envoie des signaux qui sont
capturés par un MainWindow pour lui dire que l'utilisateur commence à dessiner
ou arrête de dessiner.


La GrilleInput correspond à la grille entrée par l'utilisateur, elle dispose
d'une méthode `scale` qui permet de la transformer en GrilleScaled.
Cette GrilleScaled implémente une méthode `smooth` afin de l'adoucir pour
ensuite la comparer aux grilles de la base de données, avec les méthodes
`compare` (comparaison avec une grille), `compare_database` (comparaison avec
une base de données) et `best_match` (comparaison avec un vecteur de bases
de données).

La base de données est représentée par des fichiers, un par mouvement.
Il contient des informations telles que le ou les caractères auxquels peut
aboutir ce mouvement, le nombre de grilles dans la base, le nombre de
mouvements supposés déjà tracés et le nombre restant, ainsi que des
informations sur le point de départ du trait.
La classe Database contient des fonctions permettant d'importer ou 
d'exporter des grilles depuis ou vers ces fichiers.

Enfin, la classe MainWindow permet de superviser le tout, en appellant les
bonnes méthodes au bon moment, et également pour gérer si oui ou non
l'application doit attendre une nouvelle entrée de l'utilisateur.

Après le tracé d'un premier trait, le programme calcule le classement
de ses suppositions vis à vis de chaque lettre (grâce à la méthode
`best_match`). Si, dans les estimations les mieux classées, se trouve un
caractère à faire en plus de mouvements, le programme lance un timer lui
permettant d'attendre une nouvelle entrée utilisateur pendant un certain
temps. Si l'utilisateur entre en effet un nouveau dessin, le programme
va à nouveau calculer les meilleures estimations vis à vis du nouveau trait
en restreignant ses recherches aux caractères faisables en le nombre
de mouvements déjà effectuées, et faisables à partir du premier dessin.

Évidemment, ce procédé fonctionne pour des caractères à réaliser en un nombre
quelconque de mouvements.


## Perspectives

Avec notre méthode, il aurait été parfaitement possible de reconnaitre
d'autres alphabets tels que l'alphabet latin, chinois, égyptien antique, etc.

Nous aurions également pu ajouter une infinité d'autres smileys ou de dessins
que nous aurions traduit en UTF-8

L'algorithme de reconnaissance peut aussi être amélioré en ajustant divers
paramètres.
