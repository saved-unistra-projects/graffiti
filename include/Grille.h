#ifndef __GRILLE_H__
#define __GRILLE_H__

#include <vector>
#include "Case.h"

/* Classe abstraite de grille */
class Grille
{
  // Attributs
  protected:
    std::vector<std::vector<Case>> cases; // Tableau des cases du dessin.

  // Méthodes
  protected:
    Grille(); // Permet d'en faire une classe abstraite.

  public:
    /* Setter de case */
    void setCase(Case c, size_t x, size_t y);
    /* Getter de case */
    Case* getCase(size_t x, size_t y);
};

#endif // __GRILLE_H__
