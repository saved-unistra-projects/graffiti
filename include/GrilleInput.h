#ifndef __GRILLEINPUT_H__
#define __GRILLEINPUT_H__

#include <stdexcept>
#include <tuple>
#include "Grille.h"
#include "GrilleScaled.h"
#include "painter.h"

/* Forward definition pour éviter les dépendances circulaires*/
class GrilleScaled;

/* Classe représentant la grille d'entrée dans laquelle l'utilisateur dessine.
 * Hérite de Grille */
class GrilleInput : public Grille
{
  // Attributs
  private:
    /* Couple de position du point de départ */
    std::tuple<size_t, size_t> posPointDepart;


  // Méthodes
  public:

     /* Constructeur de la classe GrilleInput.*/
    GrilleInput(std::vector<std::vector<int>> grille);

     /* Setter pour le point de départ modificateur du point d'origine.*/
    void setPointDepart(size_t x, size_t y);
    
     /* Getter pour le couple des coordonnées du point d'origine*/
    std::tuple<size_t, size_t> getPointDepart();

    /*Fonction qui met la grille à l'échelle.
     * Renvoi une GrilleScaled correspondant à la mise à l'échelle de cette
     * grille.transforme la grille en le carré minimal contenant tout les 
     * pixels de la lettre.
    */
    GrilleScaled *scale();
  
  
  protected:

    /*
     * Fonction qui renvoi la coordonnée la plus basse de la lettre.
     * L'attribut cases ne doit pas être NULL.
     * 
     * Renvoie la coordonnée verticale minimale de l'image.
     * Si l'image est vierge, renvoie -1.
    */
    ssize_t get_min_x();

    /*
     * Fonction qui renvoi la coordonnée la plus haute de la lettre.
     * L'attribut cases ne doit pas être NULL.
     * Renvoie la coordonnée verticale maximale de l'image.
     * Si l'image est vierge, renvoie -1.
    */
    ssize_t get_max_x();

    /*
     * Fonction qui renvoi la coordonnée la plus à gauche de la lettre.
     * L'attribut cases ne doit pas être NULL.
     * 
     * Renvoie la coordonnée horizontale minimale de l'image.
     * Si l'image est vierge, renvoie -1.
    */
    ssize_t get_min_y();
    
    /*
     * Fonction qui renvoi la coordonnée la plus à droite de la lettre.
     * L'attribut cases ne doit pas être NULL.
     * Renvoie la coordonnée horizontale maximale de l'image.
     * Si l'image est vierge, renvoie -1.
    */
    ssize_t get_max_y();
};

#endif // __GRILLEINPUT_H__
