#ifndef __Histo_H__
#define __Histo_H__

#include <QtCharts/QChartView>
#include <QtCharts/QBarSeries>
#include <QtCharts/QBarSet>
#include <QtCharts/QLegend>
#include <QtCharts/QBarCategoryAxis>
#include <QtCharts/QValueAxis>
#include <QHBoxLayout>
#include <vector>

QT_CHARTS_USE_NAMESPACE

class Histo : public QWidget
{
  Q_OBJECT

  public:
    /* Constructeur de la classe Histo */
    Histo(QWidget *parent = 0);

    /* Fonction qui ajoute une barre de longueur val et de nom lettre */
    void addBar(std::string lettre, int val);

    /* Fonction qui supprime toutes les barres de l'histogramme */
    void clearBar();    


  private:
    /* Série de données ici toutes les lettres sont regroupées dans la même */
    QBarSeries *series;

    /* Représentation de l'histogramme */
    QChart *chart;

    /* Liste des lettres de la série */
    QStringList* categories;

    /* Valeurs des colonnes */
    QBarSet *valeurs;
    
    /* Axe des abscisses */
    QBarCategoryAxis *axisX;

    /* Axe des ordonnées */
    QValueAxis *axisY;

    /* Affiche l'histogramme */
    QChartView *chartView;

    /* layout de l'histogramme pour le widget */
    QHBoxLayout* layoutPainter;
};

#endif // __Histo_H__
