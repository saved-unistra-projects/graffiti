#ifndef __CASE_H__
#define __CASE_H__

#define MIN_PROBA 0
#define MAX_PROBA 100

/* Une classe représentant les case des différentes grilles*/
class Case{
  private:
    /* Valeur de la case */
    int proba;
  
  public:

    /*Le constructeur vérifie que l'argument se trouve dans 
     * la fourchette * MIN_PROBA - MAX_PROBA. Sinon,
     * il tronque la valeur.*/
    Case(int pr);

    /*Le setter tronque également la valeur si la fourchette
     * MIN_PROBA - MAX_PROBA n'est pas respectée.*/
    void setProba(int pr);

    /* Getter qui renvoie la valeur de la case */
    int getProba();
};
    

#endif //__CASE_H__
