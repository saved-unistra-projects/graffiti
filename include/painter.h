#ifndef Painter_H
#define Painter_H

#include <QColor>
#include <QImage>
#include <QPoint>
#include <QWidget>
#include <vector>

#define VALPOINTDEPART -1

class Painter : public QWidget
{
  Q_OBJECT

	public:
		/* Constructeur de la zone de dessein */
    Painter(QWidget *parent = 0);

		/* setter du nombre de lignes déjà tracées */
    int getNbStrokes();

		/* Getter du nombre de lignes déjà tracées */
    void setNbStrokes(int nb);

	signals:
		/* Signal émis lorsque l'utilisateur relache son click */
    void hasChanged(bool b);

		/* Signal émis au click de l'utilisateur */
    void ignoringState(bool b);

	public slots:
		/* Fonction qui efface l'image */
    void clearImage();

		/* Getter du tableau de pixels */
    std::vector<std::vector<int>> getPixels();

	protected:
		/* Fonction appelée au click */
    void mousePressEvent(QMouseEvent *event) override;

		/* Fonction appelée lors du déplacement de la souris */
    void mouseMoveEvent(QMouseEvent *event) override;

		/* Fonction appelée au relachement du click */
    void mouseReleaseEvent(QMouseEvent *event) override;

		/* Fonction qui trace la ligne décrite par le mouvement de la souris */
    void paintEvent(QPaintEvent *event) override;

		/* Fonction qui change la taille du widget lors d'un resize de la fenetre */
    void resizeEvent(QResizeEvent *event) override;

	private:
		/* Affiche la ligne tracée et rempli le tableau de pixels */
    void drawLineTo(const QPoint &endPoint);

		/* Redimensionne l'image de tracage */
    void resizeImage(QImage *image, const QSize &newSize);

		/* Redimensionne le tableau de pixels */
    void resizePixels(const QSize &newSize);

    QRgb background; // Background color
    QColor myPenColor; // Pen's color
    int myPenWidth; // Pen's width
    QImage image; // The actual draw
    bool scribbling; // If the user is scribbling
    QPoint lastPoint; // Position of the last clicked point
    int nbStrokes; // Number of 

    std::vector<std::vector<int>> pixels; // Pixels of the image
};

#endif
