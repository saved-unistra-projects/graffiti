#ifndef __GRILLESCALED_H__
#define __GRILLESCALED_H__

#include <vector>
#include <tuple>
#include <algorithm>
#include <system_error>
#include "Database.h"
#include "Grille.h"
#include "Database.h"

#define SIZE_X 37
#define SIZE_Y 37
#define SCORE_MIN 0
#define SCORE_MAX 1
#define SMOOTH_RATE 1.5

/* Forward declaration pour éviter les dépendances circulaires */
class Database;

/* Grille mise à une échelle connue. Hérite de grille */
class GrilleScaled : public Grille
{
  // Attributs
  protected:
    /* Tailles de la grille */
    static const size_t sizeX = SIZE_X;
    static const size_t sizeY = SIZE_Y;
    bool haut; // Le premier point est situé en haut.
    bool droite; // Le premier point est situé à droite.

  // Methodes
  public:
    /*
     * Prends un vector de vector car doit pouvoir contruire une grille à partir
     * - d'une grilleInput
     * - d'une database
    */
    GrilleScaled(std::vector<std::vector<Case>> grille);

    /*Getters et setters pour les tailles et les attributs point de départ*/
    static size_t getSizeX();
    static size_t getSizeY();
    void setHaut(bool h);
    bool getHaut();
    void setDroite(bool d);
    bool getDroite();
    
    /*
     * Donne un score de fidélité entre les grilles
     * 
     * Ajoute ou supprime des points selon si les cases sont identiques ou non
     * 
     * retourne la proba que telle grille soit égale à la grille de comparaison.
     * 
    */ 
    double compare(GrilleScaled* g);
    /*
     * Fonction de comparaison d'un grille avec une toute une base de donnée.
     * Cette fonction retourne le score maximum  obtenu parmis toutes les 
     * grilles de la base, et un booleen indiquant si le score est valide (verif
     * à l'aide du point de départ*/
    std::tuple<double,bool> compare_database(Database* db);

    /*
     * Renvoie une QString qui représente la grille */
    QString toQString();

    /*
     * Fonction qui renvoie un vecteur de tuples de string, double et int
     * représentant les caractères des bases de donnée testés triés par
     * score décroissant. L'entier représente le nombre d'étapes restantes pour
     * les lettres à dessiner en plusieurs étapes*/
    std::vector<std::tuple<std::string,double,int>> best_match(int ignore, 
	std::vector<std::string> steps, std::vector<Database*> dbv);

    /* Lisse une GrilleScaled (donne des scores décroissants autour des endroits
     * où l'utilisateur a dessiné. La fonction "affine" le dessin. */
    void smooth(); 

    /* Fonction interne utilisée par smooth*/
    private:
    void smoothie(GrilleScaled *g, size_t x, size_t y, int value, int iter);
};

#endif // __GRILLESCALED_H__
