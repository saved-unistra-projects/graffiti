#ifndef MAINWINDOW_H
#define MAINWINDOW_H

#include <QMainWindow>
#include <QWidget>
#include <QHBoxLayout>
#include <QVBoxLayout>
#include <QTextEdit>
#include <QGroupBox>
#include <QGridLayout>
#include <QFormLayout>
#include <QRadioButton>
#include <QLabel>
#include <QPushButton>
#include <QDockWidget>
#include <QCheckBox>
#include <QLineEdit>
#include <QTimer>
#include <QDir>
#include <QMessageBox>

#include <vector>
#include "painter.h"
#include "GrilleInput.h"
#include "Database.h"
#include "histo.h"

#define SEUIL_WAIT 2
#define TIME_WAIT 1500

class MainWindow : public QMainWindow
{
  Q_OBJECT

  public:
    MainWindow(QWidget *parent = 0);

  public slots:
    void showAlphabet();
    void changeAlphabet();
    void hypervisor();
    void hypervisor_wait();
    void hypervisor_end();
    void timerHandler();
    void setIgnore(bool);
    bool getIgnore();
    void setDbRep(std::string);
    std::string getDbRep();
    void write(std::string s);
    bool getVerrMaj();
    void setVerrMaj(bool b); 
    bool getLastMaj();
    void setLastMaj(bool b);


  protected:
    void resizeEvent(QResizeEvent *event) override;

  private:
    /* Attribute of the window */
    QHBoxLayout *m_mainLayout;
    QWidget *m_centralArea;

    // Left area
    QVBoxLayout *m_leftLayout;

    Painter *m_graffitiArea = NULL;

    QHBoxLayout *m_leftLayoutChild;

    QGroupBox *m_database;
    QVBoxLayout *m_layoutDatabase;
    QGridLayout *m_layoutDatabaseRow;
    QVBoxLayout *m_layoutDbSelectShow;
    QLabel *m_alphabeticLabel;
    QRadioButton *m_alphabetic;
    QLabel *m_numericLabel;
    QRadioButton *m_numeric;
    QPushButton *m_showAlphabet;

    QVBoxLayout *m_layoutHistoAddDb;

    Histo* m_histo;

    // Right area
    QVBoxLayout *m_rightLayout;
    QTextEdit *m_showText; // Display test

    // Alphabet Area
    QDockWidget *m_alphabetArea;
    QLabel *m_image;

    // Window state
    QTimer *m_timer;
    bool ignore;
    std::vector<std::tuple<std::string, double, int>> bm;
    bool timerEnded;
    std::vector<std::string> oldSteps;
    std::string dbRep;
    bool verrMaj;
    bool lastMaj;

    std::vector<Database*> dbv;
    void loadDB();
    void freeDB();


	

#ifdef ADDDB

    public slots:
	void addGrilleDb(GrilleScaled* gs);

    private:
	QGroupBox *m_groupeAddDb;
	QFormLayout *m_layoutAddDb;
	QCheckBox *m_addDatabase;
	QLineEdit *m_nameDb;
#endif
};

#endif // MAINWINDOW_H
