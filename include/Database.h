#ifndef __DATABASE_H__
#define __DATABASE_H__

#include <iostream>
#include <fstream>
#include <sstream>
#include <QFile>
#include <QTextStream>
#include <QIODevice>
#include <limits>
#include <string>
#include <string.h>
#include <vector>
#include "GrilleScaled.h"

#ifdef ADDDB
  #define DB_DIR "databases/"
#else
  #define DB_DIR ":/databases/"
#endif

#define META_DATA_LENGTH 9
#define MAX_DBN_LENGTH 6

/* Forward declaration permettant d'eviter les dépendances circulaires */
class GrilleScaled;

class Database
{
  // Attributs
  private:
    /*Chemin du fichier contenant tous les tableaux de lettres*/
    std::string source_path;

    /*Nombre de grilles représentées dans le fichier*/
    int nombre_grilles;

    /* Grilles présentes dans la database */
    std::vector<GrilleScaled*> gs;

    /*Caractère à reconnaitre*/
    std::string character;

    bool haut; // Le premier point est situé en haut.
    bool droite; // Le premier point est situé à droite.
    bool verifV; // Importance du placement vertical du premier point.
    bool verifH; // Importance du placement horizontal du premier point.
    int RemainingSteps; //Etapes restantes pour les lettres à plusieurs étapes
    int PreviousSteps; //Etapes précédentes pour les lettres à plusieurs étapes
    


  // Méthodes
  public:

    /* Constructeur initialisé à partir du chemin de la DB */
    Database(std::string path);
    
    /*Destructeur de la classe qui permet de free les grilles de la DB*/
    ~Database();

    /* Setter pour le chemin d'acces */
    void setSourcePath(std::string path);
    /* Getter pour le chemin d'acces */
    std::string getSourcePath();
    /* Setter pour le caractère à reconnaitre */
    void setCharacter(std::string character);
    /* Getter pour le caractère à reconnaitre */
    std::string getCharacter();
    /* Setter pour le nombre de grilles */
    void setNombreGrilles(int n);
    /* Getter pour le nombre de grilles */
    int getNombreGrilles();
    
    /* Setters et getters pour les différentes méta-données */
    void setHaut(bool h);
    bool getHaut();
    void setDroite(bool d);
    bool getDroite();
    void setVerifV(bool v);
    bool getVerifV();
    void setVerifH(bool h);
    bool getVerifH();
    void setRemainingSteps(int s);
    int getRemainingSteps();
    void setPreviousSteps(int s);
    int getPreviousSteps();


    /* Renvoie la GrilleScaled numéro i de la base de données */
    GrilleScaled* get_grille_scaled(int i);

    /* Ajouter une GrilleScaled à la base de données (pour le développement)*/
    void add_grille(GrilleScaled *g);

  private:
    /* Initialise la DB depuis le fichier pointé par l'attribut source_path */
    void init_database();

    /* Initialise le vecteur de grilles à partir du fichier*/
    void importFromDB();

    /*Exporte les grilles dans le fichier*/
    void exportToDB();

};


#endif //__DATABASE_H__
